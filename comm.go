package main

import (
	"fmt"
	"path/filepath"

	"go.bug.st/serial"
)

var commPort serial.Port

func sendEnableCmd(relayIdx uint) {
	enableCmd := fmt.Sprintf("wlacz %d\n", relayIdx)
	_, err := commPort.Write([]byte(enableCmd))
	if err != nil {
		panic(err)
	}
}

func sendDisableCmd(relayIdx uint) {
	disableCmd := fmt.Sprintf("wylacz %d\n", relayIdx)
	_, err := commPort.Write([]byte(disableCmd))
	if err != nil {
		panic(err)
	}
}

func getRelaysStates() [4]bool {
	checkStatusCmd := "status\n"
	validResponseHdr := "OK, stan przekaznikow: "
	respHdrLen := len(validResponseHdr)

	rxBuf := make([]byte, 64)
	_, err := commPort.Write([]byte(checkStatusCmd))
	if err != nil {
		panic(err)
	}

	_, err = commPort.Read(rxBuf)
	if err != nil {
		panic(err)
	}
	rxBuf = rxBuf[respHdrLen:]

	statuses := [4]bool{false, false, false, false}

	for i := 0; i < len(statuses); i++ {
		char := rxBuf[i*2]
		if char == '0' {
			statuses[i] = false
		} else if char == '1' {
			statuses[i] = true
		} else {
			panic("Blad w sprawdzaniu stanu przekaznikow, nieznana odpowiedz od kontrolera")
		}
	}
	return statuses
}

func portSetup(ctrlName string) {
	mode := &serial.Mode{
		BaudRate: 115200, // shouldn't really matter since other side is a USB CDC ACM device
		DataBits: 8,
		Parity:   serial.NoParity,
		StopBits: serial.OneStopBit,
	}

	devicePath, err := filepath.EvalSymlinks("/dev/serial/by-id/" + ctrlName)
	if err != nil {
		fmt.Println("Nie znaleziono urządzenia")
		panic(err)
	}

	commPort, err = serial.Open(devicePath, mode)
	if err != nil {
		fmt.Println("Błąd przy otwieraniu portu szeregowego")
		panic(err)
	}
}

func portClose() {
	commPort.Close()
}

func serialReceive(rxChan chan string) {
	rxBuf := make([]byte, 64)
	for {
		n, err := commPort.Read(rxBuf)
		if err != nil {
			fmt.Println("Błąd przy odbieraniu danych")
			panic(err)
		}
		if n == 0 {
			fmt.Println("\nEOF")
			break
		}
		var i int
		for i = 0; i < n; i++ {
			if rxBuf[i] < 32 {
				break
			}
		}
		rxChan <- string(rxBuf[:i])
	}
}
