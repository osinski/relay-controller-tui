package main

import (
	"fmt"
	"os"
	"time"

	"github.com/go-ping/ping"
)

var relayState = [...]bool{false, false, false, false}
var tui *Tui

func main() {
	controllerUsbName, TvIp, pingInterval, automaticRelayIdx := parseConfig("ustawienia.yaml")
	if automaticRelayIdx < 1 || automaticRelayIdx > 4 {
		exitOnErr("BLAD: nr automatycznego przekaznika jest poza zakresem (1-4)")
	}

	portSetup(controllerUsbName)
	defer portClose()

	consoleChannel := make(chan string)
	defer close(consoleChannel)

	relayState = getRelaysStates()
	fmt.Println(relayState)

	tui = newTui(automaticRelayIdx - 1)
	tui.consolePrintln(fmt.Sprintf("Odczytano plik konfiguracyjny,\nurządzenie: %v\nadres IP telewizora: %v, "+
		"interwal pingowania: %vs, nr automatycznego przekaznika: %v\n",
		controllerUsbName, TvIp, pingInterval, automaticRelayIdx))

	go serialReceive(consoleChannel)

	go pingTV(TvIp, time.Duration(pingInterval), automaticRelayIdx, consoleChannel)

	go func() {
		for {
			tui.consolePrintln(<-consoleChannel)
		}
	}()

	tui.run()
}

func pingTV(addrToPing string, interval time.Duration, autoRelayIdx uint, logChan chan string) {
	for {
		if tui.isAutoEnabled(autoRelayIdx - 1) {
			pinger, err := ping.NewPinger(addrToPing)
			if err != nil {
				panic(err)
			}
			pinger.Count = 3
			pinger.Timeout = 5 * time.Second

			err = pinger.Run()
			if err != nil {
				panic(err)
			}

			stats := pinger.Statistics()
			if stats.PacketsSent == stats.PacketsRecv {
				logChan <- fmt.Sprintf("Ping OK (Wyslano: %v, odebrano: %v)",
					stats.PacketsSent, stats.PacketsRecv)

				if relayState[autoRelayIdx-1] == false {
					relayState[autoRelayIdx-1] = true

					sendEnableCmd(autoRelayIdx)
					tui.btnMarkAsEnabled(autoRelayIdx - 1)
				}
			} else {
				logChan <- fmt.Sprintf("Ping Dupa (Wyslano: %v, odebrano: %v)",
					stats.PacketsSent, stats.PacketsRecv)

				if relayState[autoRelayIdx-1] == true {
					relayState[autoRelayIdx-1] = false

					sendDisableCmd(autoRelayIdx)
					tui.btnMarkAsDisabled(autoRelayIdx - 1)
				}
			}
		}
		time.Sleep(interval * time.Second)
	}
}

func exitOnErr(errstr string) {
	os.Stderr.WriteString(errstr + "\n")
	os.Exit(1)
}
