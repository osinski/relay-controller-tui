module relay-ctrl-tui

go 1.21.0

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.6.0 // indirect
	github.com/go-ping/ping v1.1.0 // indirect
	github.com/google/uuid v1.4.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/rivo/tview v0.0.0-20230826224341-9754ab44dc1c // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	go.bug.st/serial v1.6.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/sync v0.5.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/term v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
