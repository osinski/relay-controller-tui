package main

import (
	"fmt"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

type Config struct {
	RelayController struct {
		Name string `yaml:"nazwa"`
	} `yaml:"kontroler-przekaznikow"`
	TV struct {
		IP                string `yaml:"ip"`
		PingInterval      uint   `yaml:"interwal-pingowania"`
		AutomaticRelayIdx uint   `yaml:"przekaznik"`
	} `yaml:"tv"`
}

func parseConfig(configFilename string) (string, string, uint, uint) {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)

	data, err := os.ReadFile(exPath + "/" + "ustawienia.yaml")
	if err != nil {
		fmt.Println("Błąd odczytu pliku ustawienia.yaml")
		panic(err)
	}

	var cfg Config
	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		fmt.Println("Błąd parsowania pliku konfiguracyjnego")
		panic(err)
	}

	return cfg.RelayController.Name, cfg.TV.IP, cfg.TV.PingInterval, cfg.TV.AutomaticRelayIdx
}
