package main

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type TuiColors struct {
	BgDark  tcell.Color
	BgLight tcell.Color
	Fg      tcell.Color
	Yellow  tcell.Color
	Green   tcell.Color
	Red     tcell.Color
	Cyan    tcell.Color
}

var GruvboxColors = TuiColors{
	BgDark:  tcell.NewRGBColor(0x28, 0x28, 0x28),
	BgLight: tcell.NewRGBColor(0x3c, 0x38, 0x36),
	Fg:      tcell.NewRGBColor(0xdf, 0xbf, 0x8e),
	Yellow:  tcell.NewRGBColor(0xe3, 0xa8, 0x4e),
	Green:   tcell.NewRGBColor(0xa9, 0xb6, 0x65),
	Red:     tcell.NewRGBColor(0xea, 0x69, 0x62),
	Cyan:    tcell.NewRGBColor(0x89, 0xb4, 0x82),
}

type Tui struct {
	app     *tview.Application
	console *tview.TextView
	buttons [4]*tview.Button
}

func newTui(automaticRelayIdx uint) *Tui {
	// set Theme
	tview.Styles.PrimitiveBackgroundColor = GruvboxColors.BgDark
	tview.Styles.ContrastBackgroundColor = GruvboxColors.BgDark
	tview.Styles.MoreContrastBackgroundColor = GruvboxColors.BgDark
	tview.Styles.BorderColor = GruvboxColors.Yellow
	tview.Styles.TitleColor = GruvboxColors.Yellow
	tview.Styles.PrimaryTextColor = GruvboxColors.Fg
	tview.Styles.SecondaryTextColor = GruvboxColors.Fg
	tview.Styles.TertiaryTextColor = GruvboxColors.Fg
	tview.Styles.InverseTextColor = GruvboxColors.Cyan
	tview.Styles.ContrastSecondaryTextColor = GruvboxColors.Cyan

	// set up app and its widgets
	tui := Tui{app: tview.NewApplication(), console: tview.NewTextView()}

	controlGrid := tview.NewGrid().SetRows(0, 0, 0, 0)
	controlGrid.SetBorder(true).SetTitle("Sterowanie Przekaznikami")

	helpTextView := tview.NewTextView().SetDynamicColors(true).SetTextAlign(tview.AlignCenter)
	helpTextView.SetBorder(true).SetTitle("Pomoc")

	tui.console.SetTextAlign(tview.AlignLeft)
	tui.console.SetBorder(true)
	tui.console.SetTitle("Konsola")
	tui.console.SetScrollable(false)
	tui.console.SetChangedFunc(func() {
		tui.app.Draw()
	})

	newControlButton := func(text string) *tview.Button {
		button := tview.NewButton(text)
		button.SetBorder(true)
		button.SetBackgroundColorActivated(GruvboxColors.BgLight)
		button.SetLabelColorActivated(GruvboxColors.Red)
		return button
	}
	tui.buttons = [...]*tview.Button{
		newControlButton("Przekaznik 1 - WYLACZONY"),
		newControlButton("Przekaznik 2 - WYLACZONY"),
		newControlButton("Przekaznik 3 - WYLACZONY"),
		newControlButton("Przekaznik 4 - WYLACZONY"),
	}
	tui.btnMarkAsAutoCtrl(automaticRelayIdx)
	controlButtonSelectedFunc := func() {
		var relayIdx uint = 0
		focused := tui.app.GetFocus()
		for idx, primitive := range tui.buttons {
			if primitive == focused {
				relayIdx = uint(idx)
				break
			}
		}

		if relayState[relayIdx] == true {
			sendDisableCmd(relayIdx + 1)
			tui.btnMarkAsDisabled(relayIdx)
			relayState[relayIdx] = false
		} else {
			sendEnableCmd(relayIdx + 1)
			tui.btnMarkAsEnabled(relayIdx)
			relayState[relayIdx] = true
		}
	}
	for i, button := range tui.buttons {
		button.SetSelectedFunc(controlButtonSelectedFunc)
		if relayState[i] == true {
			tui.btnMarkAsEnabled(uint(i))
		} else {
			tui.btnMarkAsDisabled(uint(i))
		}
		controlGrid.AddItem(button, i, 0, 1, 1, 0, 0, false)
	}

	mainGrid := tview.NewGrid().SetRows(22, -1, 5)
	mainGrid.AddItem(controlGrid, 0, 0, 1, 1, 0, 0, true).
		AddItem(tui.console, 1, 0, 1, 1, 0, 0, false).
		AddItem(helpTextView, 2, 0, 1, 1, 0, 0, false)

	helpTextView.SetText(
		"[blue]Enter[white] - Przelacz zaznaczony przekaznik | " +
			"[blue]Strzalki[white] - Nawigacja | " +
			"[blue]Tab[white] - Przelacz miedzy panelem sterowania i konsola\n" +
			"[blue]q / Ctrl-c[white] - Zakoncz program\n" +
			fmt.Sprintf("[blue]a[white] - Wlacz/wylacz automatyczne sterowanie przekaznikiem nr %v", automaticRelayIdx+1))

	tui.app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyTab:
			switch tui.app.GetFocus() {
			case tui.buttons[0]:
				fallthrough
			case tui.buttons[1]:
				fallthrough
			case tui.buttons[2]:
				fallthrough
			case tui.buttons[3]:
				tui.app.SetFocus(tui.console)
			case tui.console:
				for idx, button := range tui.buttons {
					if !button.IsDisabled() {
						tui.app.SetFocus(tui.buttons[idx])
						break
					}
				}
			}
		case tcell.KeyUp:
			buttonIdx := len(tui.buttons)
			focused := tui.app.GetFocus()
			for idx, primitive := range tui.buttons {
				if primitive == focused {
					if idx == 0 {
						buttonIdx = len(tui.buttons) - 1
					} else {
						buttonIdx = idx - 1
					}
					break
				}
			}
			if buttonIdx < len(tui.buttons) {
				// TODO - this assumes only 1 button can be disabled,
				// redo this when it will no longer be the case
				if tui.buttons[buttonIdx].IsDisabled() {
					if buttonIdx == 0 {
						buttonIdx = len(tui.buttons) - 1
					} else {
						buttonIdx--
					}
				}
				tui.app.SetFocus(tui.buttons[buttonIdx])
			}
		case tcell.KeyDown:
			buttonIdx := len(tui.buttons)
			focused := tui.app.GetFocus()
			for idx, primitive := range tui.buttons {
				if primitive == focused {
					if idx == len(tui.buttons)-1 {
						buttonIdx = 0
					} else {
						buttonIdx = idx + 1
					}
					break
				}
			}
			if buttonIdx < len(tui.buttons) {
				// TODO - this assumes only 1 button can be disabled,
				// redo this when it will no longer be the case
				if tui.buttons[buttonIdx].IsDisabled() {
					if buttonIdx == len(tui.buttons)-1 {
						buttonIdx = 0
					} else {
						buttonIdx++
					}
				}
				tui.app.SetFocus(tui.buttons[buttonIdx])
			}
		case tcell.KeyRune:
			switch event.Rune() {
			case 'a':
				if tui.buttons[automaticRelayIdx].IsDisabled() {
					tui.btnMarkAsManualCtrl(automaticRelayIdx)
				} else {
					tui.btnMarkAsAutoCtrl(automaticRelayIdx)
				}
			case 'q':
				tui.app.Stop()
			}
		}
		return event
	})

	toFocus := 0
	for i, button := range tui.buttons {
		if !button.IsDisabled() {
			toFocus = i
			break
		}
	}
	tui.app.SetRoot(mainGrid, true).SetFocus(tui.buttons[toFocus])

	return &tui
}

func (tui *Tui) btnMarkAsAutoCtrl(idx uint) {
	button := tui.buttons[idx]
	button.SetDisabled(true)
	button.SetLabel("(AUTO) " + button.GetLabel())
}

func (tui *Tui) btnMarkAsManualCtrl(idx uint) {
	button := tui.buttons[idx]
	button.SetDisabled(false)
	label := button.GetLabel()
	label = strings.Replace(label, "(AUTO) ", "", 1)
	button.SetLabel(label)
}

func (tui *Tui) btnMarkAsEnabled(idx uint) {
	button := tui.buttons[idx]
	label := button.GetLabel()
	label = strings.Replace(label, "WYLACZONY", "WLACZONY", 1)
	button.SetLabelColor(GruvboxColors.Green)
	button.SetLabelColorActivated(GruvboxColors.Green)
	button.SetLabel(label)
}

func (tui *Tui) btnMarkAsDisabled(idx uint) {
	button := tui.buttons[idx]
	label := button.GetLabel()
	label = strings.Replace(label, "WLACZONY", "WYLACZONY", 1)
	button.SetLabelColor(GruvboxColors.Red)
	button.SetLabelColorActivated(GruvboxColors.Red)
	button.SetLabel(label)
}

func (tui *Tui) isAutoEnabled(idx uint) bool {
    return tui.buttons[idx].IsDisabled()
}

func (tui *Tui) consolePrintln(newLine string) {
	tui.console.SetText(tui.console.GetText(false) + newLine + "\n")
}

func (tui *Tui) run() {
	if err := tui.app.Run(); err != nil {
		panic(err)
	}
}
