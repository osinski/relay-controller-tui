# Kontroler przekaźników
*You can find english version below*

## Sposób użycia
### Pobranie kodu źródłowego
```shell
git clone https://gitlab.com/osinski/relay-controller-tui.git
```

### Kompilowanie i instalacja
Do uruchomienia programu niezbędne jest zainstalowanie języka `Go`, np.:
```shell
sudo pacman -S go
```

Następnie w folderze z plikami źródłowymi należy wywołać komendę:
```shell
go build -o <nazwa_pliku_wykonywalnego>
```
np.
```shell
go build -o przekazniki
```
spowoduję budowę programu i utworzenie pliku wykonywalnego o nazwie `przekazniki`.


### Plik konfiguracyjny
Należy przygotować plik z ustawieniami, zaczynając od utworzenia kopii przykładowego pliku:
```shell
cp ustawienia.yaml.przyklad ustawienia.yaml
```
a następnie jego edycji.

**UWAGA**, jeśli w folderze z programem nie będzie prawidłowego pliku `ustawienia.yaml`, program
nie uruchomi się.


# Relay Controller TUI

## How to use
